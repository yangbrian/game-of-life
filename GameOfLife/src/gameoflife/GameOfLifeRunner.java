
package gameoflife;

import info.gridworld.actor.Actor;
import info.gridworld.actor.ActorWorld;
import info.gridworld.grid.Location;
import info.gridworld.grid.UnboundedGrid;

public class GameOfLifeRunner
{
   public static void main(String[] args)
   {
      ActorWorld world = new ActorWorld(new UnboundedGrid<Actor>());
      world.add(new Location(4, 5), new Cell());
      world.add(new Location(4, 6), new Cell());
      world.add(new Location(5, 4), new Cell());
      world.add(new Location(5, 5), new Cell());
      world.add(new Location(6, 5), new Cell());
      world.show();
   }
}
