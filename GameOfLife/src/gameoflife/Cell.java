package gameoflife;

import info.gridworld.actor.*;
import info.gridworld.grid.*;
import java.util.ArrayList;

/**
 * Game of Life simulation<br/>
 * AP Computer Science Project 3 Part 3
 * @author Brian Yang
 * @version 04.07.2013
 */
public class Cell extends Actor {
    private ArrayList<Location> breedingLocations;
    private boolean die;
    private boolean planning;
    
    /**
     * Initializes the instance fields of the Cell
     */
    public Cell() {
        breedingLocations = new ArrayList<Location>();
        die = false;
        planning = true;
    }
    
    /**
     * Plans which locations should breed a new living cell 
     * and determines if the current cell should die
     */
    public void plan() {
        Grid<Actor> gr = getGrid();
        
        // reset the breeding locations
        breedingLocations = new ArrayList<Location>();
        
        // check all adjacent cells and check which ones have exactly 3 adjacent neighbors
        for (Location l : gr.getEmptyAdjacentLocations(getLocation())) {
            if (gr.getOccupiedAdjacentLocations(l).size() == 3)
                breedingLocations.add(l);
        }
        
        int adjacentLiving = gr.getOccupiedAdjacentLocations(getLocation()).size();
        
        // is the cell dead according to the conditions given?
        die = (adjacentLiving == 0 || adjacentLiving == 1) || adjacentLiving > 3;
        
    }
    
    /**
     * Does the bug live? If so, it should spawn on the grid.  
     * If not, it should be removed.
     */
    public void live() {
        Grid<Actor> gr = getGrid();
        for (Location l : breedingLocations) {
            (new Cell()).putSelfInGrid(gr, l);
        }
        if(die)
            removeSelfFromGrid();
    }
    
  /**
   * Alternates between planning phase, where cells to be born and to die
   * are determined, and the live phase, where cells are born and die.
   */
   @Override
   public void act() {
       if (planning)
           plan();
       else
           live();
       
       planning = !planning;
   }
}